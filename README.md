#Getting started#
You will need the following tools:

- Git
- Apache Maven

1. Download the following repositories:
    - bpm-bdd-starter
    - bpm-workflow-common
    - bpm-utils2

    Do this using Git as follows:
```.sh
        $ git clone https://[your user name]@bitbucket.org/centrum/bpm-workflow-common.git
        $ git clone https://[your user name]@bitbucket.org/edwardwhiting/bpm-bdd-starter.git
        $ git clone https://[your user name]@bitbucket.org/centrum/bpm-utils2.git
```

1. Make sure that the following environment variables are set:

    - JAVA_HOME (eg C:\Program Files\Java\jdk1.8.0_25)
    - M2_HOME (eg C:\tools\apache-maven-3.2.3)

1. Make sure the PATH environment variable contains:

    - the path to the Java virtual machine (**%JAVA_HOME%\bin**)
    - the path to the Maven binary (**%M2_HOME%\bin**), and
    - the path to your web driver (eg for the Chrome webdriver it might be )

1. Make sure you have the right web driver installed and that the property to record its location is set in **[project directory]/thucydides.properties**.  Eg for Google Chrome, set **webdriver.chrome.driver** to something like **C:/Program Files (x86)/Google/Chrome/chromedriver.exe**.

1. Install the utils with the following commands: 
```.sh
    $ cd bpm-utils2
    $ mvn install
```

1. Install the common workflow library with the following commands:
```.sh
    $ cd ../bpm-workflow-common
    $ mvn install
```

1. The tests invoke a process application on a BPM Server.  The server URL is set in **[project directory]/thucydides.properties** and the default value is **ec2-54-179-177-222.ap-southeast-1.compute.amazonaws.com**.

1. The usernames which the tests can use to login are defined in **[project directory]/src/test/resources/users.properties**: these usernames must be defined in the server also. 

1. The project which the test invokes is called **BDD Starter** and the BPD is **Test Some Tasks**.  The process application id and the BPD id which the test invokes are hardcoded in **[project directory]/src/test/java/au/com/centrumsystems/bpmtesting/jbehave/TestSomeTasks.java**. 

1. To run the tests run (from bpm-bdd) :
```.sh
$ mvn install
```