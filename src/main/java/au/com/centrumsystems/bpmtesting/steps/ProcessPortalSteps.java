package au.com.centrumsystems.bpmtesting.steps;

import au.com.centrumsystems.bpmtesting.common.domain.DisplayedTask;
import au.com.centrumsystems.bpmtesting.common.steps.ProcessPortalBaseSteps;
import net.thucydides.core.annotations.Step;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by john on 18/09/2014.
 */
public class ProcessPortalSteps extends ProcessPortalBaseSteps {

    @Step
    public void waitForCompletedTasksToAppear() {
        waitForCompletedTasksToAppear(30);
    }

    private void waitForCompletedTasksToAppear(int remainingTries) {
        List<DisplayedTask> tasks = getDisplayedTasks();
        if (remainingTries == 0 && tasks.isEmpty()) {
            throw new AssertionError("Expecting a matching completed task in the task list");
        }
        if (tasks.isEmpty()) {
            taskListPage.waitFor(1).second();
            taskListPage.listOpenTasks();
            taskListPage.listCompletedTasks();
            waitForCompletedTasksToAppear(remainingTries - 1);
        }
    }

    @Step
    public void shouldSeeFormTabWithError(String tabTitle) {
        assertThat(taskListPage.getTabsWithErrors()).contains(tabTitle);
    }

    @Step
    public void shouldSeeErrorInForm(String tabLabel, String message) {
        taskListPage.selectTab(tabLabel);
        assertThat(taskListPage.getDisplayedErrorMessages()).contains(message);
    }
}
