package au.com.centrumsystems.bpmtesting.jbehave.stepdefinitions;

import au.com.centrumsystems.bpmtesting.steps.ProcessPortalSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Then;

/**
 * Created by john on 23/09/2014.
 */
public class SubmittingApplicationDetailsStepDefinitions {

    @Steps
    ProcessPortalSteps portalUser;

    @Then("the task should appear in the Completed Tasks list")
    public void shouldAppearInTheCompletedList() {
        portalUser.opens_task_list();
        portalUser.lists_completed_tasks();
        portalUser.filters_tasks_by(ProcessManager.getCreatedProcessId());
        portalUser.waitForCompletedTasksToAppear();
    }

    @Then("the '<form>' tab should display an error message '<message>'")
    public void formShouldDisplayErrorMessage(String form, String message) {
        portalUser.shouldSeeFormTabWithError(form);
        portalUser.shouldSeeErrorInForm(form, message);
    }

    @Then("the '$tabTitle' tab should indicate missing data")
    public void shouldIndicateMissingDataIn(String tabTitle) {
        portalUser.shouldSeeFormTabWithError(tabTitle);
    }

}
