package au.com.centrumsystems.bpmtesting.jbehave;

import au.com.centrumsystems.bpmtesting.common.pages.BPMTaskListPage;
import au.com.centrumsystems.bpmtesting.jbehave.stepdefinitions.ProcessManager;
import au.com.centrumsystems.bpmtesting.steps.ProcessPortalSteps;
import net.thucydides.core.annotations.Steps;
import net.thucydides.jbehave.ThucydidesJUnitStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;

/**
 * Created by john on 18/09/2014.
 * Modified by Edward.
 */
public class TestSomeTasks extends ThucydidesJUnitStory{


    @Steps
    ProcessPortalSteps portalUser;

    private static final String BPD_ID = "25.775eedfc-5fa6-4686-8a8a-476029359048";
    private static final String PROCESS_APP_ID = "2066.8034678b-1c59-4bf9-b01f-7cce4e9a0893";

    @Given("a $processName process has been created")
    public void createApplicationDetailsProcess(String processName) throws Exception {
        ProcessManager.createProcess(processName, BPD_ID, PROCESS_APP_ID);
    }

    @When("$user submits valid data")
    public void submitValidData(final String user) {
        portalUser.submit_task();
    }
}
