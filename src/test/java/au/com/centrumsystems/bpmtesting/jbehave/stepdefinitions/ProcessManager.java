package au.com.centrumsystems.bpmtesting.jbehave.stepdefinitions;

import au.com.centrumsystems.bpmtesting.common.domain.BPMUser;
import au.com.centrumsystems.bpmtesting.common.domain.SessionVariables;
import au.com.centrumsystems.bpmtesting.common.infrastructure.BPMProcessManager;
import bpm.rest.client.BPMClientException;
import bpm.rest.client.authentication.AuthenticationTokenHandlerException;
import net.thucydides.core.Thucydides;

import java.net.MalformedURLException;

/**
 * Created by john on 7/10/2014.
 */
public class ProcessManager {

    private final String bpdId;
    private final String processAppId;

    public ProcessManager(String bpdId, String processAppId) {
        this.bpdId = bpdId;
        this.processAppId = processAppId;
    }


    public static void createProcess(String processName, String bpdId, String processAppId) throws BPMClientException, AuthenticationTokenHandlerException, MalformedURLException {
        BPMProcessManager processManager = new BPMProcessManager(bpdId, processAppId);
        String pid = processManager.createProcessCalled(processName);

        Thucydides.getCurrentSession().put("processManager",processManager);
        Thucydides.getCurrentSession().put(SessionVariables.PID,pid);
    }

    public static String getProcessOwner(BPMUser bpmUser) throws BPMClientException, AuthenticationTokenHandlerException, MalformedURLException {
        String pid = Thucydides.getCurrentSession().get(SessionVariables.PID).toString();
        BPMProcessManager processManager = getProcessManager();
        return processManager.getProcessOwner(pid, bpmUser.getUsername(), bpmUser.getPassword());

    }

    private static BPMProcessManager getProcessManager() {
        return (BPMProcessManager) Thucydides.getCurrentSession().get("processManager");
    }

    public static void assignCurrentTaskToUser(BPMUser bpmUser) throws BPMClientException, AuthenticationTokenHandlerException, MalformedURLException {
        String pid = Thucydides.getCurrentSession().get(SessionVariables.PID).toString();
        int taskId = getProcessManager().getCurrentTaskId(pid);
        getProcessManager().assignToUser(taskId, bpmUser.getUsername());
    }

    public static  String getCreatedProcessId() {
        return Thucydides.getCurrentSession().get(SessionVariables.PID).toString();
    }

}
