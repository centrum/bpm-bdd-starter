Meta:

Narrative:
In order to work on my assigned tasks
As a BPM user
I want to be able to view the task details.

Scenario: Submitting a Sample User Task
Given a Sample User Task process has been created
And John has claimed this task
When John submits valid data
Then the task should appear in the Completed Tasks list
